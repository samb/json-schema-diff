"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _ = require("lodash");
const keyword_defaults_1 = require("../set/keyword-defaults");
const set_of_subsets_1 = require("../set/set-of-subsets");
const object_subset_1 = require("../set/subset/object-subset");
const is_type_supported_1 = require("./is-type-supported");
const supportsAllObjects = (objectSetParsedKeywords) => {
    // tslint:disable:cyclomatic-complexity
    const everyPropertyIsAll = Object.keys(objectSetParsedKeywords.properties)
        .every((propertyName) => objectSetParsedKeywords.properties[propertyName].type === 'all');
    return everyPropertyIsAll
        && _.isEqual(objectSetParsedKeywords.required, keyword_defaults_1.defaultRequired)
        && objectSetParsedKeywords.additionalProperties.type === 'all'
        && objectSetParsedKeywords.minProperties === keyword_defaults_1.defaultMinProperties
        && objectSetParsedKeywords.maxProperties === keyword_defaults_1.defaultMaxProperties;
};
const createObjectSubset = (objectSetParsedKeywords) => {
    if (!is_type_supported_1.isTypeSupported(objectSetParsedKeywords.type, 'object')) {
        return object_subset_1.emptyObjectSubset;
    }
    if (supportsAllObjects(objectSetParsedKeywords)) {
        return object_subset_1.allObjectSubset;
    }
    return object_subset_1.createObjectSubsetFromConfig(objectSetParsedKeywords);
};
exports.createObjectSet = (objectSetParsedKeywords) => {
    const objectSubset = createObjectSubset(objectSetParsedKeywords);
    return new set_of_subsets_1.SetOfSubsets('object', [objectSubset]);
};

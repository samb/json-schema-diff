"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.defaultProperties = {};
exports.defaultMaxItems = Infinity;
exports.defaultMaxProperties = Infinity;
exports.defaultMinItems = 0;
exports.defaultMinProperties = 0;
exports.defaultRequired = [];
exports.defaultTypes = ['array', 'boolean', 'integer', 'null', 'number', 'object', 'string'];

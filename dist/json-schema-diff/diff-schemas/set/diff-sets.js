"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.diffSets = (sourceSet, destinationSet, logSet) => {
    const intersectionOfSets = sourceSet.intersect(destinationSet);
    logSet('intersectionOfSets', intersectionOfSets);
    const intersectionOfSetsComplement = intersectionOfSets.complement();
    logSet('intersectionOfSetsComplement', intersectionOfSetsComplement);
    const addedToDestinationSet = intersectionOfSetsComplement.intersect(destinationSet);
    logSet('addedToDestinationSet', addedToDestinationSet);
    const removedFromDestinationSet = intersectionOfSetsComplement.intersect(sourceSet);
    logSet('removedFromDestinationSet', removedFromDestinationSet);
    return { addedToDestinationSet, removedFromDestinationSet };
};

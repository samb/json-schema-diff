"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isCoreRepresentationJsonSchema = (schema) => !!schema;

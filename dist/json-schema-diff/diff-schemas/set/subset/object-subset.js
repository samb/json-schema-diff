"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const complement_object_subset_config_1 = require("./object-subset/complement-object-subset-config");
const intersect_object_subset_config_1 = require("./object-subset/intersect-object-subset-config");
const object_subset_config_has_contradictions_1 = require("./object-subset/object-subset-config-has-contradictions");
const simplify_object_subset_config_1 = require("./object-subset/simplify-object-subset-config");
const subset_1 = require("./subset");
class SomeObjectSubset {
    constructor(config) {
        this.config = config;
        this.setType = 'object';
        this.type = 'some';
    }
    static toSchemaProperties(properties) {
        const schemaProperties = {};
        for (const propertyName of Object.keys(properties)) {
            schemaProperties[propertyName] = properties[propertyName].toJsonSchema();
        }
        return schemaProperties;
    }
    get properties() {
        return this.config.properties;
    }
    complement() {
        return complement_object_subset_config_1.complementObjectSubsetConfig(this.config).map(exports.createObjectSubsetFromConfig);
    }
    intersect(other) {
        return other.intersectWithSome(this);
    }
    intersectWithSome(other) {
        return exports.createObjectSubsetFromConfig(intersect_object_subset_config_1.intersectObjectSubsetConfig(this.config, other.config));
    }
    toJsonSchema() {
        return Object.assign({ additionalProperties: this.config.additionalProperties.toJsonSchema(), maxProperties: this.config.maxProperties, minProperties: this.config.minProperties, properties: SomeObjectSubset.toSchemaProperties(this.config.properties), required: this.config.required, type: ['object'] }, this.notJsonSchema());
    }
    notJsonSchema() {
        if (this.config.not) {
            return {
                not: {
                    additionalProperties: this.config.not.additionalProperties.toJsonSchema(),
                    properties: SomeObjectSubset.toSchemaProperties(this.config.not.properties),
                    type: ['object']
                }
            };
        }
        return {};
    }
}
exports.allObjectSubset = new subset_1.AllSubset('object');
exports.emptyObjectSubset = new subset_1.EmptySubset('object');
exports.createObjectSubsetFromConfig = (config) => {
    const simplifiedConfig = simplify_object_subset_config_1.simplifyObjectSubsetConfig(config);
    return object_subset_config_has_contradictions_1.objectSubsetConfigHasContradictions(simplifiedConfig)
        ? exports.emptyObjectSubset
        : new SomeObjectSubset(simplifiedConfig);
};

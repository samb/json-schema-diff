"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const isItemsAndMinItemsContradiction = (config) => {
    const itemsAcceptsNoValues = config.items.type === 'empty';
    const minItemsRequiresValues = config.minItems > 0;
    return itemsAcceptsNoValues && minItemsRequiresValues;
};
const isMaxItemsAndMinItemsContradiction = (config) => config.minItems > config.maxItems;
const isMinItemsContradiction = (config) => config.minItems === Infinity;
const isItemsAndNotItemsContradiction = (config) => config.notItems ? config.items.equal(config.notItems) : false;
const contradictionTests = [
    isItemsAndMinItemsContradiction,
    isMaxItemsAndMinItemsContradiction,
    isMinItemsContradiction,
    isItemsAndNotItemsContradiction
];
exports.arraySubsetConfigHasContradictions = (config) => contradictionTests.some((contradictionTest) => contradictionTest(config));

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const json_set_1 = require("../../json-set");
const keyword_defaults_1 = require("../../keyword-defaults");
const maxItemsAllowsNoItems = (config) => config.maxItems === 0;
const notItemsDisallowsEmptyArrays = (config) => config.notItems !== undefined && config.notItems.type === 'empty';
exports.simplifyArraySubsetConfig = (config) => {
    if (maxItemsAllowsNoItems(config)) {
        return Object.assign({}, config, { items: json_set_1.emptyJsonSet, maxItems: keyword_defaults_1.defaultMaxItems });
    }
    if (notItemsDisallowsEmptyArrays(config)) {
        return Object.assign({}, config, { notItems: undefined, minItems: 1 });
    }
    return config;
};

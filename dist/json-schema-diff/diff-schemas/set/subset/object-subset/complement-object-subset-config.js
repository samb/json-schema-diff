"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const json_set_1 = require("../../json-set");
const keyword_defaults_1 = require("../../keyword-defaults");
const object_subset_config_1 = require("./object-subset-config");
const complementProperties = (config) => object_subset_config_1.getPropertyNames(config).map((propertyName) => {
    const complementedPropertySet = object_subset_config_1.getPropertySet(config, propertyName).complement();
    return {
        additionalProperties: json_set_1.allJsonSet,
        maxProperties: keyword_defaults_1.defaultMaxProperties,
        minProperties: keyword_defaults_1.defaultMinProperties,
        properties: { [propertyName]: complementedPropertySet },
        required: [propertyName]
    };
});
const complementRequiredProperties = (config) => config.required.map((requiredPropertyName) => ({
    additionalProperties: json_set_1.allJsonSet,
    maxProperties: keyword_defaults_1.defaultMaxProperties,
    minProperties: keyword_defaults_1.defaultMinProperties,
    properties: {
        [requiredPropertyName]: json_set_1.emptyJsonSet
    },
    required: keyword_defaults_1.defaultRequired
}));
const complementMinProperties = (config) => ({
    additionalProperties: json_set_1.allJsonSet,
    maxProperties: config.minProperties - 1,
    minProperties: keyword_defaults_1.defaultMinProperties,
    properties: keyword_defaults_1.defaultProperties,
    required: keyword_defaults_1.defaultRequired
});
const complementMaxProperties = (config) => ({
    additionalProperties: json_set_1.allJsonSet,
    maxProperties: keyword_defaults_1.defaultMaxProperties,
    minProperties: config.maxProperties + 1,
    properties: keyword_defaults_1.defaultProperties,
    required: keyword_defaults_1.defaultRequired
});
const complementAdditionalProperties = (config) => ({
    additionalProperties: json_set_1.allJsonSet,
    maxProperties: keyword_defaults_1.defaultMaxProperties,
    minProperties: keyword_defaults_1.defaultMinProperties,
    not: {
        additionalProperties: config.additionalProperties,
        properties: config.properties
    },
    properties: keyword_defaults_1.defaultProperties,
    required: keyword_defaults_1.defaultRequired
});
exports.complementObjectSubsetConfig = (config) => {
    const complementedProperties = complementProperties(config);
    const complementedAdditionalProperties = complementAdditionalProperties(config);
    const complementedRequiredProperties = complementRequiredProperties(config);
    const complementedMinProperties = complementMinProperties(config);
    const complementedMaxProperties = complementMaxProperties(config);
    return [
        complementedAdditionalProperties,
        ...complementedProperties,
        ...complementedRequiredProperties,
        complementedMinProperties,
        complementedMaxProperties
    ];
};

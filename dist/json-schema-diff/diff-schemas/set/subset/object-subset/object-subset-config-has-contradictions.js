"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const object_subset_config_1 = require("./object-subset-config");
const isMinPropertiesBiggerThanDefinedProperties = (config) => {
    const numberOfDefinedPropertiesInSchema = Object.keys(config.properties)
        .map((propertyName) => config.properties[propertyName])
        .filter((propertySchema) => propertySchema.type !== 'empty')
        .length;
    return config.minProperties > numberOfDefinedPropertiesInSchema;
};
const areAdditionalPropertiesNotAllowed = (config) => config.additionalProperties.type === 'empty';
const isMinPropertiesAndAdditionalPropertiesContradiction = (config) => areAdditionalPropertiesNotAllowed(config) && isMinPropertiesBiggerThanDefinedProperties(config);
const isRequiredAndPropertiesAndAdditionalPropertiesContradiction = (config) => config.required.some((propertyName) => object_subset_config_1.getPropertySet(config, propertyName).type === 'empty');
const isMinPropertiesAndMaxPropertiesContradiction = (config) => config.minProperties > config.maxProperties;
const isMinPropertiesContradiction = (config) => config.minProperties === Infinity;
const isMaxPropertiesAndRequiredContradiction = (config) => config.required.length > config.maxProperties;
const isAdditionalPropertiesAndNotAdditionalPropertiesContradiction = (config) => 
// TODO: When we support 'not', does this need to look at not.properties?
config.not ? config.additionalProperties.equal(config.not.additionalProperties) : false;
const contradictionTests = [
    isRequiredAndPropertiesAndAdditionalPropertiesContradiction,
    isMinPropertiesAndAdditionalPropertiesContradiction,
    isMinPropertiesAndMaxPropertiesContradiction,
    isMinPropertiesContradiction,
    isMaxPropertiesAndRequiredContradiction,
    isAdditionalPropertiesAndNotAdditionalPropertiesContradiction
];
exports.objectSubsetConfigHasContradictions = (config) => contradictionTests.some((contradictionTest) => contradictionTest(config));

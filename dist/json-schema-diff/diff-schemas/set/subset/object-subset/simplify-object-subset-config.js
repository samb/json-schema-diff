"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const json_set_1 = require("../../json-set");
const keyword_defaults_1 = require("../../keyword-defaults");
const maxPropertiesAllowsNoProperties = (config) => config.maxProperties === 0;
const notDisallowsObjectsWithNoProperties = (config) => config.not !== undefined
    && config.not.additionalProperties.type === 'empty'
    && Object.keys(config.not.properties).length === 0;
exports.simplifyObjectSubsetConfig = (config) => {
    if (maxPropertiesAllowsNoProperties(config)) {
        return Object.assign({}, config, { maxProperties: keyword_defaults_1.defaultMaxProperties, additionalProperties: json_set_1.emptyJsonSet });
    }
    if (notDisallowsObjectsWithNoProperties(config)) {
        return Object.assign({}, config, { not: undefined, minProperties: 1 });
    }
    return config;
};

import {DiffTestCase, registerDiffTestCases} from '../support/register-diff-test-cases';

describe('diff-schemas type array', () => {
    describe('items', () => {
        const testCases: DiffTestCase[] = [
            {
                description: 'array with items constraint has items constraint made more restrictive',
                examples: [[], [1], ['a'], [1, 'a'], [true], [true, 1], 'a'],
                input: {
                    a: {
                        items: {
                            type: ['string', 'number']
                        },
                        type: 'array'
                    },
                    b: {
                        items: {
                            type: 'number'
                        },
                        type: 'array'
                    }
                },
                output: {
                    added: false,
                    removed: {
                        items: {
                            type: ['number', 'string']
                        },
                        not: {
                            items: {
                                type: ['number']
                            },
                            type: ['array']
                        },
                        type: ['array']
                    }
                }
            },
            {
                description: 'numbers and array gets numbers constrained and items constraint',
                examples: [[], 1, [1], ['a'], [1, 'a'], ['a', 'a'], [1, 1], 'foo'],
                input: {
                    a: {
                        type: ['array', 'number']
                    },
                    b: {
                        items: {type: 'number'},
                        type: ['array']
                    }
                },
                output: {
                    added: false,
                    removed: {
                        not: {
                            items: {type: ['number']},
                            type: ['array']
                        },
                        type: ['array', 'number']
                    }
                }
            },
            {
                description: 'array with items constraint has items constraint changed',
                examples: [[], [1], ['a'], [1, 'a'], [true], [true, 1], 'a'],
                input: {
                    a: {
                        items: {
                            type: 'number'
                        },
                        type: 'array'
                    },
                    b: {
                        items: {
                            type: 'string'
                        },
                        type: 'array'
                    }
                },
                output: {
                    added: {
                        items: {
                            type: ['string']
                        },
                        minItems: 1,
                        type: ['array']
                    },
                    removed: {
                        items: {
                            type: ['number']
                        },
                        minItems: 1,
                        type: ['array']
                    }
                }
            },
            {
                description: 'remove array with items constraint',
                examples: [[], [1], ['a'], [1, 'a'], 'a'],
                input: {
                    a: {
                        items: {
                            type: 'number'
                        },
                        type: 'array'
                    },
                    b: false
                },
                output: {
                    added: false,
                    removed: {
                        items: {
                            type: ['number']
                        },
                        type: ['array']
                    }
                }
            },
            {
                description: 'unconstrained array has items constraint added',
                examples: [[], [1], ['a'], [1, 'a'], 'a'],
                input: {
                    a: {
                        type: 'array'
                    },
                    b: {
                        items: {
                            type: 'string'
                        },
                        type: 'array'
                    }
                },
                output: {
                    added: false,
                    removed: {
                        not: {
                            items: {
                                type: ['string']
                            },
                            type: ['array']
                        },
                        type: ['array']
                    }
                }
            }
        ];

        registerDiffTestCases(testCases);
    });

    describe('minItems', () => {
        const testCases: DiffTestCase[] = [
            {
                description: 'array with minItems constraint removed',
                examples: [[], [1], [1, 2], 'a'],
                input: {
                    a: {
                        minItems: 1,
                        type: 'array'
                    },
                    b: false
                },
                output: {
                    added: false,
                    removed: {
                        minItems: 1,
                        type: ['array']
                    }
                }
            },
            {
                description: 'unconstrained array constrained with minItems',
                examples: [[], [1], [1, 2], 'a'],
                input: {
                    a: {
                        type: 'array'
                    },
                    b: {
                        minItems: 2,
                        type: 'array'
                    }
                },
                output: {
                    added: false,
                    removed: {
                        maxItems: 1,
                        type: ['array']
                    }
                }
            },
            {
                description: 'array constrained with minItems has minItems constraint made more restrictive',
                examples: [[], [1], [1, 2], [1, 2, 3], [1, 2, 3, 4], [1, 2, 3, 4, 5], 'a'],
                input: {
                    a: {
                        minItems: 2,
                        type: 'array'
                    },
                    b: {
                        minItems: 4,
                        type: 'array'
                    }
                },
                output: {
                    added: false,
                    removed: {
                        maxItems: 3,
                        minItems: 2,
                        type: ['array']
                    }
                }
            },
            {
                description: 'empty array support in the result is represented as items=false',
                examples: [[], [1]],
                input: {
                    a: {
                        type: 'array'
                    },
                    b: {
                        minItems: 1,
                        type: 'array'
                    }
                },
                output: {
                    added: false,
                    removed: {
                        items: false,
                        type: ['array']
                    }
                }
            }
        ];

        registerDiffTestCases(testCases);
    });

    describe('maxItems', () => {
        const testCases: DiffTestCase[] = [
            {
                description: 'array with maxItems constraint removed altogether',
                examples: [[], [1, 2], [1, 2, 3], 'foo'],
                input: {
                    a: {
                        maxItems: 2,
                        type: 'array'
                    },
                    b: false
                },
                output: {
                    added: false,
                    removed: {
                        maxItems: 2,
                        type: ['array']
                    }
                }
            },
            {
                description: 'unconstrained array constrained with maxItems',
                examples: [[], [1, 2], [1, 2, 3], [1, 2, 3, 4], 'foo'],
                input: {
                    a: {
                        type: 'array'
                    },
                    b: {
                        maxItems: 2,
                        type: 'array'
                    }
                },
                output: {
                    added: false,
                    removed: {
                        minItems: 3,
                        type: ['array']
                    }
                }
            },
            {
                description: 'array constrained with maxItems has maxItems constraint made less restrictive',
                examples: [[], [1], [1, 2], [1, 2, 3], [1, 2, 3, 4], [1, 2, 3, 4, 5]],
                input: {
                    a: {
                        maxItems: 1,
                        type: 'array'
                    },
                    b: {
                        maxItems: 4,
                        type: 'array'
                    }
                },
                output: {
                    added: {
                        maxItems: 4,
                        minItems: 2,
                        type: ['array']
                    },
                    removed: false
                }
            }
        ];

        registerDiffTestCases(testCases);
    });

    describe('items + minItems', () => {
        const testCases: DiffTestCase[] = [
            {
                description: 'array constrained with items and minItems has the items constraint made less restrictive',
                examples: [[], [1], ['foo'], [1, 2], ['foo', 'bar'], [1, 'foo'], 'foo'],
                input: {
                    a: {
                        items: {type: ['string', 'number']},
                        minItems: 2,
                        type: 'array'
                    },
                    b: {
                        items: {type: 'string'},
                        minItems: 2,
                        type: 'array'
                    }
                },
                output: {
                    added: false,
                    removed: {
                        items: {
                            type: ['number', 'string']
                        },
                        minItems: 2,
                        not: {
                            items: {
                                type: ['string']
                            },
                            type: ['array']
                        },
                        type: ['array']
                    }
                }
            },
            {
                description: 'array constrained with contradicting minItems and items',
                examples: [[], [1], 'foo'],
                input: {
                    a: {
                        items: false,
                        minItems: 1,
                        type: 'array'
                    },
                    b: false
                },
                output: {
                    added: false,
                    removed: false
                }
            },
            {
                description: 'array constrained with contradicting minItems and items has minItems removed',
                examples: [[], [1], 'foo'],
                input: {
                    a: {
                        items: false,
                        minItems: 1,
                        type: 'array'
                    },
                    b: {
                        items: false,
                        type: 'array'
                    }
                },
                output: {
                    added: {
                        items: false,
                        type: ['array']
                    },
                    removed: false
                }
            }
        ];

        registerDiffTestCases(testCases);
    });

    describe('items + maxItems', () => {
        const testCases: DiffTestCase[] = [
            {
                description: 'different ways of representing arrays with no items',
                examples: [[], [1], 'foo'],
                input: {
                    a: {
                        maxItems: 0,
                        type: 'array'
                    },
                    b: {
                        items: false,
                        type: 'array'
                    }
                },
                output: {
                    added: false,
                    removed: false
                }
            },
            {
                description: 'array with no items has items constraint relaxed and the maxItems constraint restricted',
                examples: [[], [1], [1, 2], [1, 2, 3], [1, 2, 3, 4], 'foo'],
                input: {
                    a: {
                        items: false,
                        type: 'array'
                    },
                    b: {
                        maxItems: 3,
                        type: 'array'
                    }
                },
                output: {
                    added: {
                        maxItems: 3,
                        minItems: 1,
                        type: ['array']
                    },
                    removed: false
                }
            }
        ];

        registerDiffTestCases(testCases);
    });

    describe('minItems + maxItems', () => {
        const testCases: DiffTestCase[] = [
            {
                description: 'array range is expanded in both directions',
                examples: [
                    [], [1], [1, 2], [1, 2, 3], [1, 2, 3, 4], [1, 2, 3, 4, 5], [1, 2, 3, 4, 5, 6],
                    [1, 2, 3, 4, 5, 6, 7], [1, 2, 3, 4, 5, 6, 7, 8], 'foo'
                ],
                input: {
                    a: {
                        maxItems: 5,
                        minItems: 3,
                        type: 'array'
                    },
                    b: {
                        maxItems: 7,
                        minItems: 1,
                        type: 'array'
                    }
                },
                output: {
                    added: {
                        anyOf: [
                            {
                                maxItems: 2,
                                minItems: 1,
                                type: ['array']
                            },
                            {
                                maxItems: 7,
                                minItems: 6,
                                type: ['array']
                            }
                        ]
                    },
                    removed: false
                }
            }
        ];

        registerDiffTestCases(testCases);
    });

    describe('items + minItems + maxItems', () => {
        const testCases: DiffTestCase[] = [
            {
                description: 'array items, minItems and maxItems constraints are all made less restrictive',
                examples: [
                    [], [1], [1, 2], [1, 2, 3], [1, 2, 3, 4], [1, 2, 3, 4, 5], [1, 2, 3, 4, 5, 6],
                    [1, 2, 3, 4, 5, 6, 7], [1, 2, 3, 4, 5, 6, 7, 8], ['a'], ['a', 'b'], ['a', 'b', 'c'],
                    ['a', 'b', 'c', 'd'], ['a', 'b', 'c', 'd', 'e'], ['a', 'b', 'c', 'd', 'e', 'f'],
                    ['a', 'b', 'c', 'd', 'e', 'f', 'g'], ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'], [1, 'a'],
                    [1, 2, 'a'], [1, 2, 3, 'a'], [1, 2, 3, 4, 'a'], [1, 2, 3, 4, 5, 'a'], [1, 2, 3, 4, 5, 6, 'a'],
                    [1, 2, 3, 4, 5, 6, 7, 'a'], 'foo'
                ],
                input: {
                    a: {
                        items: {
                            type: ['string']
                        },
                        maxItems: 5,
                        minItems: 3,
                        type: 'array'
                    },
                    b: {
                        maxItems: 7,
                        minItems: 1,
                        type: 'array'
                    }
                },
                output: {
                    added: {
                        anyOf: [
                            {
                                maxItems: 7,
                                minItems: 1,
                                not: {
                                    items: {
                                        type: ['string']
                                    },
                                    type: ['array']
                                },
                                type: ['array']
                            },
                            {
                                maxItems: 2,
                                minItems: 1,
                                type: ['array']
                            },
                            {
                                maxItems: 7,
                                minItems: 6,
                                type: ['array']
                            }
                        ]
                    },
                    removed: false
                }
            }
        ];

        registerDiffTestCases(testCases);
    });
});

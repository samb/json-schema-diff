import {DiffTestCase, registerDiffTestCases} from '../support/register-diff-test-cases';

describe('diff-schemas type', () => {
    const testCases: DiffTestCase[] = [
        {
            description: 'types are the same',
            examples: ['foo', 1],
            input: {
                a: {type: 'string'},
                b: {type: 'string'}
            },
            output: {
                added: false,
                removed: false
            }
        },
        {
            description: 'type as array vs single type that are the same',
            examples: ['foo', 1],
            input: {
                a: {type: 'string'},
                b: {type: ['string']}
            },
            output: {
                added: false,
                removed: false
            }
        },
        {
            description: 'type number is changed',
            examples: ['foo', 1, true],
            input: {
                a: {type: ['string', 'number']},
                b: {type: ['string']}
            },
            output: {
                added: false,
                removed: {type: ['number']}
            }
        },
        {
            description: 'type array is changed',
            examples: ['foo', [], 1],
            input: {
                a: {type: ['string', 'array']},
                b: {type: ['string']}
            },
            output: {
                added: false,
                removed: {type: ['array']}
            }
        },
        {
            description: 'type boolean is changed',
            examples: ['foo', true, 1],
            input: {
                a: {type: ['string', 'boolean']},
                b: {type: ['string']}
            },
            output: {
                added: false,
                removed: {type: ['boolean']}
            }
        },
        {
            description: 'type integer is changed',
            examples: ['foo', 1, []],
            input: {
                a: {type: ['string', 'integer']},
                b: {type: ['string']}
            },
            output: {
                added: false,
                removed: {type: ['integer']}
            }
        },
        {
            description: 'type null is changed',
            examples: ['foo', null, 1],
            input: {
                a: {type: ['string', 'null']},
                b: {type: ['string']}
            },
            output: {
                added: false,
                removed: {type: ['null']}
            }
        },
        {
            description: 'type object is changed',
            examples: ['foo', {}, 1],
            input: {
                a: {type: ['string', 'object']},
                b: {type: ['string']}
            },
            output: {
                added: false,
                removed: {type: ['object']}
            }
        },
        {
            description: 'type string is changed',
            examples: [1, 'foo', true],
            input: {
                a: {type: ['number', 'string']},
                b: {type: ['number']}
            },
            output: {
                added: false,
                removed: {type: ['string']}
            }
        },
        {
            description: 'type added and removed',
            examples: [1, 'foo', true],
            input: {
                a: {type: 'number'},
                b: {type: 'string'}
            },
            output: {
                added: {type: ['string']},
                removed: {type: ['number']}
            }
        },
        {
            description: 'multiple types added and removed',
            examples: [1, 'foo', true, []],
            input: {
                a: {type: ['number', 'string', 'boolean']},
                b: {type: ['number']}
            },
            output: {
                added: false,
                removed: {type: ['boolean', 'string']}
            }
        },
        {
            description: 'empty schemas',
            examples: [1, 'foo'],
            input: {
                a: {},
                b: {}
            },
            output: {
                added: false,
                removed: false
            }
        },
        {
            description: 'empty schema vs list of types',
            examples: [1, 1.1, {}, null, true, [], 'foo'],
            input: {
                a: {},
                b: {type: ['integer', 'number', 'object', 'null', 'boolean', 'array']}
            },
            output: {
                added: false,
                removed: {type: ['string']}
            }
        }
    ];

    registerDiffTestCases(testCases);
});

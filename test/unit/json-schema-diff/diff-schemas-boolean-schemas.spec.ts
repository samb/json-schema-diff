import {DiffTestCase, registerDiffTestCases} from '../support/register-diff-test-cases';

describe('diff-schemas boolean schemas', () => {
    const testCases: DiffTestCase[] = [
        {
            description: 'true schema to true schema',
            examples: ['a', 1],
            input: {
                a: true,
                b: true
            },
            output: {
                added: false,
                removed: false
            }
        },
        {
            description: 'false schema to true schema',
            examples: ['a', 1],
            input: {
                a: false,
                b: true
            },
            output: {
                added: true,
                removed: false
            }
        },
        {
            description: 'all but one type schema to true schema',
            examples: ['a', 1, {}],
            input: {
                a: {type: ['string', 'array', 'number', 'integer', 'boolean', 'null']},
                b: true
            },
            output: {
                added: {type: ['object']},
                removed: false
            }
        },
        {
            description: 'single type schema to false',
            examples: ['a', 1],
            input: {
                a: {type: 'string'},
                b: false
            },
            output: {
                added: false,
                removed: {type: ['string']}
            }
        }
    ];

    registerDiffTestCases(testCases);
});

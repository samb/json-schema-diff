'use strict';

const gulp = require('gulp');

const bump = require('gulp-bump');
const clean = require('gulp-clean');
const colors = require('ansi-colors');
const conventionalChangelog = require('gulp-conventional-changelog');
const exec = require('child_process').exec;
const filter = require('gulp-filter');
const fs = require('fs');
const git = require('gulp-git');
const jasmine = require('gulp-jasmine');
const minimist = require('minimist');
const ts = require('gulp-typescript');
const tslint = require('gulp-tslint');

const options = minimist(process.argv.slice(2), {strings: ['type']});
const tsProjectBuildOutput = ts.createProject('tsconfig.json', {noEmit: false});
const unitTests = 'build-output/test/unit/**/*.spec.js';
const e2eTests = 'build-output/test/e2e/**/*.spec.js';

const utilities = {
    compileBuildOutput: () => {
        const tsResult = tsProjectBuildOutput.src().pipe(tsProjectBuildOutput());
        return tsResult.js.pipe(gulp.dest('build-output'));
    },
    exec: (command) => {
        return new Promise((resolve, reject) => {
            console.log(`Executing command '${colors.yellow(command)}'`);
            const childProcess = exec(command, (error) => {
                if (error) {
                    reject(error);
                } else {
                    resolve();
                }
            });
            childProcess.stdout.pipe(process.stdout);
            childProcess.stderr.pipe(process.stderr);
        });
    },
    getBumpType: () => {
        const validTypes = ['major', 'minor', 'patch', 'prerelease'];
        if (validTypes.indexOf(options.type) === -1) {
            throw new Error(
                `You must specify a release type as one of (${validTypes.join(', ')}), e.g. "--type minor"`
            );
        }
        return options.type;
    },
    getPackageJsonVersion: () => {
        return JSON.parse(fs.readFileSync('./package.json', 'utf8')).version;
    }
};

const bumpVersion = () =>
    gulp.src(['./package.json'])
        .pipe(bump({type: utilities.getBumpType()}))
        .pipe(gulp.dest('./'));

const cleanBuildOutput = () =>
    gulp.src('build-output', {force: true, read: false, allowEmpty: true}).pipe(clean());

const cleanDist = () =>
    gulp.src('dist', {force: true, read: false, allowEmpty: true}).pipe(clean());

const changelog = () => {
    return gulp.src('CHANGELOG.md')
        .pipe(conventionalChangelog({preset: 'angular'}))
        .pipe(gulp.dest('./'));
};

const commitRelease = () =>
    gulp.src('.')
        .pipe(git.add({args: '--all'}))
        .pipe(git.commit(`chore: release ${utilities.getPackageJsonVersion()}`));

const compileAndUnitTest = () =>
    utilities.compileBuildOutput()
        .pipe(filter([unitTests]))
        .pipe(jasmine({includeStackTrace: true}));

const copyBuildOutputPackageJson = () =>
    gulp.src('package.json').pipe(gulp.dest('build-output'));

const compileDist = () => {
    const tsProjectDist = ts.createProject('tsconfig.json', {noEmit: false});
    const tsResult = gulp.src('lib/**/*.ts').pipe(tsProjectDist());
    return tsResult.js.pipe(gulp.dest('dist'));
};

const createNewTag = (callback) => {
    const version = utilities.getPackageJsonVersion();
    git.tag(version, `Created Tag for version: ${version}`, callback);
};

const lintCommits = () =>
    utilities.exec('./node_modules/.bin/conventional-changelog-lint --from=HEAD~20 --preset angular');

const lintTypescript = () =>
    tsProjectBuildOutput.src()
        .pipe(tslint({formatter: 'verbose'}))
        .pipe(tslint.report());

const npmPublish = () => utilities.exec('npm publish');

const pushChanges = (callback) => {
    git.push('origin', 'master', {args: '--tags'}, callback);
};

const test = () => gulp.src([unitTests, e2eTests]).pipe(jasmine({includeStackTrace: true}));

const testE2e = () => gulp.src([e2eTests]).pipe(jasmine({includeStackTrace: true}));

const watchAndRunUnitTests = () => {
    gulp.watch(['lib/**/*.ts', 'test/**/*.ts'], compileAndUnitTest)
};

const watchAndRunE2eTests = () => {
    const e2eFiles = ['build-output/lib/**/*', 'build-output/test/e2e/**/*', 'test/e2e/**/*.json'];
    gulp.watch(e2eFiles, testE2e);
};

const cleanCopyAndCompileBuildOutput = gulp.series(
    cleanBuildOutput,
    gulp.parallel(copyBuildOutputPackageJson, utilities.compileBuildOutput)
);

const lint = gulp.parallel(lintTypescript, lintCommits);

exports.default = gulp.series(
    cleanCopyAndCompileBuildOutput,
    gulp.parallel(lint, test)
);

exports.release = gulp.series(
    exports.default,
    cleanDist,
    compileDist,
    bumpVersion,
    changelog,
    commitRelease,
    createNewTag,
    pushChanges,
    npmPublish
);

exports.watch = gulp.series(
    cleanCopyAndCompileBuildOutput,
    watchAndRunUnitTests
);

exports.watchE2e = watchAndRunE2eTests;

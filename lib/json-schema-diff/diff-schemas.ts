import {DiffResult} from '../api-types';
import {logSetDebug} from './common/logger';
import {dereferenceSchema} from './diff-schemas/dereference-schema';
import {parseAsJsonSet} from './diff-schemas/parse-as-json-set';
import {diffSets} from './diff-schemas/set/diff-sets';
import {validateSchemas} from './diff-schemas/validate-schemas';

export const diffSchemas = async (sourceSchema: any, destinationSchema: any): Promise<DiffResult> => {
    const [dereferencedSourceSchema, dereferencedDestinationSchema] = await Promise.all([
        dereferenceSchema(sourceSchema), dereferenceSchema(destinationSchema)
    ]);

    await validateSchemas(sourceSchema, destinationSchema);

    const sourceSet = parseAsJsonSet(dereferencedSourceSchema);
    logSetDebug('sourceSet', sourceSet);
    const destinationSet = parseAsJsonSet(dereferencedDestinationSchema);
    logSetDebug('destinationSet', destinationSet);

    const {addedToDestinationSet, removedFromDestinationSet} = diffSets(sourceSet, destinationSet, logSetDebug);

    return {
        addedJsonSchema: addedToDestinationSet.toJsonSchema(),
        additionsFound: addedToDestinationSet.type !== 'empty',
        removalsFound: removedFromDestinationSet.type !== 'empty',
        removedJsonSchema: removedFromDestinationSet.toJsonSchema()
    };
};

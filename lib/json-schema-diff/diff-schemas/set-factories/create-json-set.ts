import {SimpleTypes} from 'json-schema-spec-types';
import {allJsonSet, createJsonSetFromConfig, emptyJsonSet} from '../set/json-set';
import {Set} from '../set/set';
import {ParsedPropertiesKeyword} from '../set/subset/object-subset/object-subset-config';
import {createArraySet} from './create-array-set';
import {createObjectSet} from './create-object-set';
import {createTypeSet} from './create-type-set';

export interface ParsedSchemaKeywords {
    additionalProperties: Set<'json'>;
    items: Set<'json'>;
    maxItems: number;
    maxProperties: number;
    minItems: number;
    minProperties: number;
    properties: ParsedPropertiesKeyword;
    required: string[];
    type: SimpleTypes[];
}

export const createJsonSetFromParsedSchemaKeywords = (parsedSchemaKeywords: ParsedSchemaKeywords): Set<'json'> =>
    createJsonSetFromConfig({
        array: createArraySet(parsedSchemaKeywords),
        boolean: createTypeSet('boolean', parsedSchemaKeywords.type),
        integer: createTypeSet('integer', parsedSchemaKeywords.type),
        null: createTypeSet('null', parsedSchemaKeywords.type),
        number: createTypeSet('number', parsedSchemaKeywords.type),
        object: createObjectSet(parsedSchemaKeywords),
        string: createTypeSet('string', parsedSchemaKeywords.type)
    });

export const createAllJsonSet = (): Set<'json'> => allJsonSet;

export const createEmptyJsonSet = (): Set<'json'> => emptyJsonSet;

import {SimpleTypes} from 'json-schema-spec-types';
import * as _ from 'lodash';
import {defaultMaxProperties, defaultMinProperties, defaultRequired} from '../set/keyword-defaults';
import {Set, Subset} from '../set/set';
import {allObjectSubset, createObjectSubsetFromConfig, emptyObjectSubset} from '../set/subset/object-subset';
import {ParsedPropertiesKeyword} from '../set/subset/object-subset/object-subset-config';
import {createTypeSetFromSubsets} from '../set/type-set';
import {isTypeSupported} from './is-type-supported';

export interface ObjectSetParsedKeywords {
    additionalProperties: Set<'json'>;
    maxProperties: number;
    minProperties: number;
    properties: ParsedPropertiesKeyword;
    required: string[];
    type: SimpleTypes[];
}

const supportsAllObjects = (objectSetParsedKeywords: ObjectSetParsedKeywords): boolean => {
    // tslint:disable:cyclomatic-complexity
    const everyPropertyIsAll = Object.keys(objectSetParsedKeywords.properties)
        .every((propertyName) => objectSetParsedKeywords.properties[propertyName].type === 'all');

    return everyPropertyIsAll
        && _.isEqual(objectSetParsedKeywords.required, defaultRequired)
        && objectSetParsedKeywords.additionalProperties.type === 'all'
        && objectSetParsedKeywords.minProperties === defaultMinProperties
        && objectSetParsedKeywords.maxProperties === defaultMaxProperties;
};

const createObjectSubset = (objectSetParsedKeywords: ObjectSetParsedKeywords): Subset<'object'> => {
    if (!isTypeSupported(objectSetParsedKeywords.type, 'object')) {
        return emptyObjectSubset;
    }

    if (supportsAllObjects(objectSetParsedKeywords)) {
        return allObjectSubset;
    }

    return createObjectSubsetFromConfig(objectSetParsedKeywords);
};

export const createObjectSet = (objectSetParsedKeywords: ObjectSetParsedKeywords): Set<'object'> => {
    const objectSubset = createObjectSubset(objectSetParsedKeywords);
    return createTypeSetFromSubsets('object', [objectSubset]);
};

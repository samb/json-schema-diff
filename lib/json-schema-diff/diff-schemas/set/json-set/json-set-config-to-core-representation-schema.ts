import {SimpleTypes} from 'json-schema-spec-types';
import {CoreRepresentationJsonSchema, isCoreRepresentationJsonSchema, RepresentationJsonSchema, Set} from '../set';
import {JsonSetConfig} from './json-set-config';
import {omitDefaults} from './omit-defaults';

const createEmptyCoreRepresentationJsonSchema = (): CoreRepresentationJsonSchema => ({type: []});

const getJsonSchemaTypeOrEmpty = (jsonSchema: CoreRepresentationJsonSchema): SimpleTypes[] => jsonSchema.type || [];

const getJsonSchemaAnyOfOrEmpty = (jsonSchema: CoreRepresentationJsonSchema): RepresentationJsonSchema[] =>
    jsonSchema.anyOf || [];

const isSimpleSchema = (schema: CoreRepresentationJsonSchema): boolean => !schema.anyOf || schema.anyOf.length <= 1;

const mergeCoreRepresentationJsonSchemas = (
    schema: CoreRepresentationJsonSchema,
    otherSchema: CoreRepresentationJsonSchema
): CoreRepresentationJsonSchema => {
    const schemaTypes = getJsonSchemaTypeOrEmpty(schema);
    const otherSchemaTypes = getJsonSchemaTypeOrEmpty(otherSchema);
    const type: SimpleTypes[] = schemaTypes.concat(otherSchemaTypes);

    const mergedSchema = {
        ...schema,
        ...otherSchema,
        type
    };

    if (schema.not && otherSchema.not) {
        mergedSchema.not = mergeCoreRepresentationJsonSchemas(schema.not, otherSchema.not);
    }

    return mergedSchema;
};

const toCoreRepresentationJsonSchema = (schema: RepresentationJsonSchema): CoreRepresentationJsonSchema =>
    isCoreRepresentationJsonSchema(schema)
        ? schema
        : createEmptyCoreRepresentationJsonSchema();

const getSubsetAsCoreRepresentationJsonSchema = <T>(typeSet: Set<T>): CoreRepresentationJsonSchema =>
    toCoreRepresentationJsonSchema(typeSet.toJsonSchema());

export const jsonSetConfigToCoreRepresentationSchema = (config: JsonSetConfig): CoreRepresentationJsonSchema => {
    const typeSetSchemas = Object
        .keys(config)
        .map((typeSetName: keyof JsonSetConfig) => getSubsetAsCoreRepresentationJsonSchema(config[typeSetName]));

    const mergedSimpleSubsetSchemas = typeSetSchemas
        .filter(isSimpleSchema)
        .map((schema): CoreRepresentationJsonSchema => {
            if (schema.anyOf) {
                return toCoreRepresentationJsonSchema(schema.anyOf[0]);
            }

            return schema;
        })
        .reduce((mergedSchema, schema) => {
            return mergeCoreRepresentationJsonSchemas(mergedSchema, schema);
        }, createEmptyCoreRepresentationJsonSchema());

    const mergedComplexSubsetSchemas = typeSetSchemas
        .filter((schema) => !isSimpleSchema(schema))
        .reduce<CoreRepresentationJsonSchema>((mergedSchema, schema) => {
            const mergedSchemaAnyOf = getJsonSchemaAnyOfOrEmpty(mergedSchema);
            const schemaAnyOf = getJsonSchemaAnyOfOrEmpty(schema);
            return {
                anyOf: mergedSchemaAnyOf.concat(schemaAnyOf)
            };
        }, {});

    let result: CoreRepresentationJsonSchema;

    if (getJsonSchemaAnyOfOrEmpty(mergedComplexSubsetSchemas).length === 0) {
        result = mergedSimpleSubsetSchemas;
    } else if (getJsonSchemaTypeOrEmpty(mergedSimpleSubsetSchemas).length === 0) {
        result = mergedComplexSubsetSchemas;
    } else {
        const mergedAnyOf = getJsonSchemaAnyOfOrEmpty(mergedComplexSubsetSchemas);
        mergedAnyOf.push(mergedSimpleSubsetSchemas);
        result = mergedComplexSubsetSchemas;
    }

    return omitDefaults(result);
};

// tslint:disable:max-classes-per-file

import {SimpleTypes} from 'json-schema-spec-types';
import * as _ from 'lodash';
import {RepresentationJsonSchema, Set, Subset} from './set';

interface TypeSet<T extends SimpleTypes> extends Set<T> {
    intersectWithSome(other: SomeTypeSet<T>): Set<T>;
    unionWithSome(other: SomeTypeSet<T>): Set<T>;
}

class AllTypeSet<T extends SimpleTypes> implements TypeSet<T> {
    public readonly type = 'all';

    public constructor(public readonly setType: T) {
    }

    public intersect(other: TypeSet<T>): TypeSet<T> {
        return other;
    }

    public intersectWithSome(other: SomeTypeSet<T>): Set<T> {
        return other;
    }

    public union(): TypeSet<T> {
        return this;
    }

    public unionWithSome(): Set<T> {
        return this;
    }

    public complement(): TypeSet<T> {
        return new EmptyTypeSet(this.setType);
    }

    public equal(): boolean {
        throw new Error('Not Implemented');
    }

    public toJsonSchema(): RepresentationJsonSchema {
        return {type: [this.setType]};
    }
}

class EmptyTypeSet<T extends SimpleTypes> implements TypeSet<T> {
    public readonly type = 'empty';

    public constructor(public readonly setType: T) {
    }

    public intersect(): TypeSet<T> {
        return this;
    }

    public intersectWithSome(): Set<T> {
        return this;
    }

    public union(other: TypeSet<T>): TypeSet<T> {
        return other;
    }

    public unionWithSome(other: SomeTypeSet<T>): Set<T> {
        return other;
    }

    public complement(): TypeSet<T> {
        return new AllTypeSet(this.setType);
    }

    public equal(): boolean {
        throw new Error('Not Implemented');
    }

    public toJsonSchema(): RepresentationJsonSchema {
        return false;
    }
}

class SomeTypeSet<T extends SimpleTypes> implements TypeSet<T> {
    public static intersectSubsets<T>(subsetListA: Array<Subset<T>>, subsetListB: Array<Subset<T>>): Array<Subset<T>> {
        const intersectedSubsets = [];

        for (const subsetA of subsetListA) {
            for (const subsetB of subsetListB) {
                intersectedSubsets.push(subsetA.intersect(subsetB));
            }
        }

        return intersectedSubsets;
    }

    public readonly type = 'some';

    public constructor(public readonly setType: T, private readonly subsets: Array<Subset<T>>) {}

    public complement(): Set<T> {
        const [head, ...tail] = this.subsets.map((set) => set.complement());
        const complementedSubsets = tail.reduce(SomeTypeSet.intersectSubsets, head);
        return createTypeSetFromSubsets(this.setType, complementedSubsets);
    }

    public intersect(other: TypeSet<T>): Set<T> {
        return other.intersectWithSome(this);
    }

    public intersectWithSome(other: SomeTypeSet<T>): Set<T> {
        const intersectedSubsets = SomeTypeSet.intersectSubsets(this.subsets, other.subsets);
        return createTypeSetFromSubsets(this.setType, intersectedSubsets);
    }

    public union(other: TypeSet<T>): Set<T> {
        return other.unionWithSome(this);
    }

    public unionWithSome(other: SomeTypeSet<T>): Set<T> {
        const newSubsets = [...this.subsets, ...other.subsets];
        return createTypeSetFromSubsets(this.setType, newSubsets);
    }

    public equal(): boolean {
        throw new Error('Not Implemented');
    }

    public toJsonSchema(): RepresentationJsonSchema {
        const schemas = this.subsets.map((subset) => subset.toJsonSchema());
        const dedupedSchemas = _.uniqWith(schemas, _.isEqual);
        return {anyOf: dedupedSchemas};
    }
}

const isAnySubsetTypeAll = <T>(subsets: Array<Subset<T>>): boolean => subsets.some((subset) => subset.type === 'all');

const isTypeAll = <T>(subsets: Array<Subset<T>>): boolean => {
    return isAnySubsetTypeAll(subsets);
};

const getNonEmptySubsets = <T>(subsets: Array<Subset<T>>): Array<Subset<T>> =>
    subsets.filter((subset) => subset.type !== 'empty');

export const createTypeSetFromSubsets = <T extends SimpleTypes>(setType: T, subsets: Array<Subset<T>>) => {
    const notEmptySubsets = getNonEmptySubsets(subsets);

    if (notEmptySubsets.length === 0) {
        return new EmptyTypeSet(setType);
    }

    if (isTypeAll(subsets)) {
        return new AllTypeSet(setType);
    }

    return new SomeTypeSet(setType, notEmptySubsets);
};

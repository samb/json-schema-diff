import {emptyJsonSet} from '../../json-set';
import {defaultMaxItems} from '../../keyword-defaults';
import {ArraySubsetConfig} from './array-subset-config';

const maxItemsAllowsNoItems = (config: ArraySubsetConfig): boolean => config.maxItems === 0;

const notItemsDisallowsEmptyArrays = (config: ArraySubsetConfig): boolean =>
    config.notItems !== undefined && config.notItems.type === 'empty';

export const simplifyArraySubsetConfig = (config: ArraySubsetConfig): ArraySubsetConfig => {
    if (maxItemsAllowsNoItems(config)) {
        return {...config, items: emptyJsonSet, maxItems: defaultMaxItems};
    }

    if (notItemsDisallowsEmptyArrays(config)) {
        return {...config, notItems: undefined, minItems: 1};
    }

    return config;
};

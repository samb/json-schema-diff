import {emptyJsonSet} from '../../json-set';
import {defaultMaxProperties} from '../../keyword-defaults';
import {ObjectSubsetConfig} from './object-subset-config';

const maxPropertiesAllowsNoProperties = (config: ObjectSubsetConfig): boolean => config.maxProperties === 0;

const notDisallowsObjectsWithNoProperties = (config: ObjectSubsetConfig): boolean =>
    config.not !== undefined
    && config.not.additionalProperties.type === 'empty'
    && Object.keys(config.not.properties).length === 0;

export const simplifyObjectSubsetConfig = (config: ObjectSubsetConfig): ObjectSubsetConfig => {
    if (maxPropertiesAllowsNoProperties(config)) {
        return {...config, maxProperties: defaultMaxProperties, additionalProperties: emptyJsonSet};
    }

    if (notDisallowsObjectsWithNoProperties(config)) {
        return {...config, not: undefined, minProperties: 1};
    }

    return config;
};

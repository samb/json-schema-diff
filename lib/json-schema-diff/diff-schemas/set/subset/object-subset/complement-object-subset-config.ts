import {allJsonSet, emptyJsonSet} from '../../json-set';
import {defaultMaxProperties, defaultMinProperties, defaultProperties, defaultRequired} from '../../keyword-defaults';
import {getPropertyNames, getPropertySet, ObjectSubsetConfig} from './object-subset-config';

const complementProperties = (config: ObjectSubsetConfig): ObjectSubsetConfig[] =>
    getPropertyNames(config).map((propertyName) => {
        const complementedPropertySet = getPropertySet(config, propertyName).complement();

        return {
            additionalProperties: allJsonSet,
            maxProperties: defaultMaxProperties,
            minProperties: defaultMinProperties,
            properties: {[propertyName]: complementedPropertySet},
            required: [propertyName]
        };
    });

const complementRequiredProperties = (config: ObjectSubsetConfig): ObjectSubsetConfig[] =>
    config.required.map((requiredPropertyName) => ({
        additionalProperties: allJsonSet,
        maxProperties: defaultMaxProperties,
        minProperties: defaultMinProperties,
        properties: {
            [requiredPropertyName]: emptyJsonSet
        },
        required: defaultRequired
    }));

const complementMinProperties = (config: ObjectSubsetConfig): ObjectSubsetConfig => ({
    additionalProperties: allJsonSet,
    maxProperties: config.minProperties - 1,
    minProperties: defaultMinProperties,
    properties: defaultProperties,
    required: defaultRequired
});

const complementMaxProperties = (config: ObjectSubsetConfig): ObjectSubsetConfig => ({
    additionalProperties: allJsonSet,
    maxProperties: defaultMaxProperties,
    minProperties: config.maxProperties + 1,
    properties: defaultProperties,
    required: defaultRequired
});

const complementAdditionalProperties = (config: ObjectSubsetConfig): ObjectSubsetConfig => ({
    additionalProperties: allJsonSet,
    maxProperties: defaultMaxProperties,
    minProperties: defaultMinProperties,
    not: {
        additionalProperties: config.additionalProperties,
        properties: config.properties
    },
    properties: defaultProperties,
    required: defaultRequired
});

export const complementObjectSubsetConfig = (config: ObjectSubsetConfig): ObjectSubsetConfig[] => {
    const complementedProperties = complementProperties(config);
    const complementedAdditionalProperties = complementAdditionalProperties(config);
    const complementedRequiredProperties = complementRequiredProperties(config);
    const complementedMinProperties = complementMinProperties(config);
    const complementedMaxProperties = complementMaxProperties(config);

    return [
        complementedAdditionalProperties,
        ...complementedProperties,
        ...complementedRequiredProperties,
        complementedMinProperties,
        complementedMaxProperties
    ];
};

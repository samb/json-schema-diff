import {SetLogger} from '../../common/logger';
import {Set} from './set';

interface DiffSetsResult<T> {
    addedToDestinationSet: Set<T>;
    removedFromDestinationSet: Set<T>;
}

export const diffSets = <T> (sourceSet: Set<T>, destinationSet: Set<T>, logSet: SetLogger): DiffSetsResult<T> => {
    const intersectionOfSets = sourceSet.intersect(destinationSet);
    logSet('intersectionOfSets', intersectionOfSets);
    const intersectionOfSetsComplement = intersectionOfSets.complement();
    logSet('intersectionOfSetsComplement', intersectionOfSetsComplement);
    const addedToDestinationSet = intersectionOfSetsComplement.intersect(destinationSet);
    logSet('addedToDestinationSet', addedToDestinationSet);
    const removedFromDestinationSet = intersectionOfSetsComplement.intersect(sourceSet);
    logSet('removedFromDestinationSet', removedFromDestinationSet);

    return {addedToDestinationSet, removedFromDestinationSet};
};
